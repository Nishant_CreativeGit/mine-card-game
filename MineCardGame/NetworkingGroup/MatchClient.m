#import "MatchClient.h"


// For State in order to know, object on which state.
typedef enum
{
    ClientStateIdle,
    ClientStateSearchingForServers,
    ClientStateConnecting,
    ClientStateConnected,
} ClientState;

@implementation MatchClient
{
    ClientState _clientState;
    NSString * _serverPeerID;
}


-(id)init
{
    if (self = [super init])
    {
        _clientState = ClientStateIdle;
    }
    return self;
}

-(void)startSearchingForServersWithSessionID:(NSString *)sessionID
{
    if(_clientState == ClientStateIdle)
    {
        _clientState = ClientStateSearchingForServers;
        _availableServers = [[NSMutableArray alloc] initWithCapacity:10];
        _session = [[GKSession alloc] initWithSessionID:sessionID displayName:nil sessionMode:GKSessionModeClient];
        _session.delegate = self;
        _session.available = YES;
    }
}

#pragma mark - GKSessionDelegate
- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state
{
#ifdef DEBUG
    NSLog(@"MatchmakingClient: peer %@ changed state %d", peerID, state);
#endif
    
    switch (state)
    {
            // The client has discovered a new server.
        case GKPeerStateAvailable:
            if(_clientState == ClientStateSearchingForServers)
            {
                if (![_availableServers containsObject:peerID])
                {
                    [_availableServers addObject:peerID];
                    [self.delegateMatchMaking matchmakingClient:self serverBecameAvailable:peerID];
                }
            }
            break;
            
            // The client sees that a server goes away.
        case GKPeerStateUnavailable:
            if(_clientState == ClientStateSearchingForServers)
            {
                if ([_availableServers containsObject:peerID])
                {
                    [_availableServers removeObject:peerID];
                    [self.delegateMatchMaking matchmakingClient:self serverBecameUnavailable:peerID];
                }
            }            
            break;
            
        case GKPeerStateConnected:
            break;
            
        case GKPeerStateDisconnected:
            break;
            
        case GKPeerStateConnecting:
            break;
    }	

}

- (void)session:(GKSession *)session didReceiveConnectionRequestFromPeer:(NSString *)peerID
{
#ifdef DEBUG
    NSLog(@"MatchmakingClient: connection request from peer %@", peerID);
#endif
}

- (void)session:(GKSession *)session connectionWithPeerFailed:(NSString *)peerID withError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"MatchmakingClient: connection with peer %@ failed %@", peerID, error);
#endif
}

- (void)session:(GKSession *)session didFailWithError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"MatchmakingClient: session failed %@", error);
#endif
}

- (NSUInteger)availableServerCount
{
    return [_availableServers count];
}

- (NSString *)peerIDForAvailableServerAtIndex:(NSUInteger)index
{
    return [_availableServers objectAtIndex:index];
}

- (NSString *)displayNameForPeerID:(NSString *)peerID
{
    return [_session displayNameForPeer:peerID];
}

-(void)connectToServerWithPeerID:(NSString *)peerId
{
    _clientState = ClientStateConnecting;
    _serverPeerID = peerId;
    [_session connectToPeer:peerId withTimeout:_session.disconnectTimeout];
}

@end
