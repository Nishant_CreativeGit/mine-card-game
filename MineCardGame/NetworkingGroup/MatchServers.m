#import "MatchServers.h"



typedef enum
{
    ServerStateIdle,
    ServerAcceptingConnection,
    serverIgnoringTheNewConnection
} ServerState;

@implementation MatchServers
{
    NSMutableArray *_conneactedClients;
    ServerState _serverState;
}
@synthesize delegateMatchMaking = _delegateMatchMaking;

-(id)init
{
    if (self = [super init])
    {
        _serverState = ServerStateIdle;
    }
    return self;
}


-(void)startAcceptingConnectionsForSessionID:(NSString *)sessionID
{
    if(_serverState == ServerStateIdle)
    {
        _serverState = ServerAcceptingConnection;
        _conneactedClients = [[NSMutableArray alloc] initWithCapacity:self.maxClients];
        _session = [[GKSession alloc] initWithSessionID:sessionID displayName:nil sessionMode:GKSessionModeServer];
        _session.delegate = self;
        _session.available = YES;
    }
}

-(NSArray *)connectedClients
{
    return _conneactedClients;
}

#pragma mark - GKSessionDelegate

- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state
{
#ifdef DEBUG
    NSLog(@"MatchmakingServer: peer %@ changed state %d", peerID, state);
#endif
    
    switch (state)
    {
        case GKPeerStateAvailable:
            break;
            
        case GKPeerStateUnavailable:
            break;
            
            // A new client has connected to the server.
        case GKPeerStateConnected:
            if (_serverState == ServerAcceptingConnection)
            {
                if (![_conneactedClients containsObject:peerID])
                {
                    [_conneactedClients addObject:peerID];
                    [self.delegateMatchMaking matchmakingServer:self clientDidConnect:peerID];
                }
            }
            break;
            
            // A client has disconnected from the server.
        case GKPeerStateDisconnected:
            if (_serverState != ServerStateIdle)
            {
                if ([_conneactedClients containsObject:peerID])
                {
                    [_conneactedClients removeObject:peerID];
                    [self.delegateMatchMaking matchmakingServer:self clientDidDisconnect:peerID];
                }
            }
            break;
            
        case GKPeerStateConnecting:
            break;
    }

}

- (void)session:(GKSession *)session didReceiveConnectionRequestFromPeer:(NSString *)peerID
{
#ifdef DEBUG
    NSLog(@"MatchmakingServer: connection request from peer %@", peerID);
#endif
    
    if (_serverState == ServerAcceptingConnection && [self connectedClientCount] < self.maxClients)
    {
        NSError *error;
        if ([session acceptConnectionFromPeer:peerID error:&error])
            NSLog(@"MatchmakingServer: Connection accepted from peer %@", peerID);
        else
            NSLog(@"MatchmakingServer: Error accepting connection from peer %@, %@", peerID, error);
    }
    else  // not accepting connections or too many clients
    {
        [session denyConnectionFromPeer:peerID];
    }
}

- (void)session:(GKSession *)session connectionWithPeerFailed:(NSString *)peerID withError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"MatchmakingServer: connection with peer %@ failed %@", peerID, error);
#endif
}

- (void)session:(GKSession *)session didFailWithError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"MatchmakingServer: session failed %@", error);
#endif
}


- (NSUInteger)connectedClientCount
{
    return [_conneactedClients count];
}

- (NSString *)peerIDForConnectedClientAtIndex:(NSUInteger)index
{
    return [_conneactedClients objectAtIndex:index];
}

- (NSString *)displayNameForPeerID:(NSString *)peerID
{
    return [_session displayNameForPeer:peerID];
}



@end
