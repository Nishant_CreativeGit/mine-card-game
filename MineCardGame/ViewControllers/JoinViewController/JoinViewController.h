#import <UIKit/UIKit.h>
#import "MatchClient.h"
@class JoinViewController;

@protocol joinViewControllerDelegate <NSObject>

-(void) joinViewControllerDidCancel :(JoinViewController *)controller;

@end



@interface JoinViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, MatchmakingClientDelegate>
{
    MatchClient *_matchMakingClient;
}

@property (strong, nonatomic) IBOutlet UITableView *joinTableView;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;

- (IBAction)closeButtonAction:(id)sender;

@property (nonatomic, strong) id <joinViewControllerDelegate>delegateJoinVC;
@end
