//
//  AppDelegate.h
//  MineCardGame
//
//  Created by cgsadmin on 3/23/16.
//  Copyright © 2016 cgsadmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

