#import <UIKit/UIKit.h>

@interface UIFont (gameFileAdditions)

+ (id) cg_fontWithSize : (CGFloat)size;

@end
