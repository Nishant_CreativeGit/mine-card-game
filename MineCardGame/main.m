//
//  main.m
//  MineCardGame
//
//  Created by cgsadmin on 3/23/16.
//  Copyright © 2016 cgsadmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
