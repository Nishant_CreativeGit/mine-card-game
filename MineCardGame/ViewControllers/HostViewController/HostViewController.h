#import <UIKit/UIKit.h>
#import "MatchServers.h"
@class HostViewController;


@protocol hostViewControllerDelegate <NSObject>

-(void)hostViewControllerDidCancel : (HostViewController*)controller;

@end

@interface HostViewController : UIViewController<MatchmakingServerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    MatchServers *_matchMakingServer;
}
@property (strong, nonatomic) IBOutlet UITableView *hostTableView;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (nonatomic,strong) id <hostViewControllerDelegate> delegateHostVC;
- (IBAction)closeButtonAction:(id)sender;


@end
