#import "MainScreenViewController.h"
#import "UIFont+gameFileAdditions.h"
#import "HostViewController.h"
#import "JoinViewController.h"

@interface MainScreenViewController ()

@end

@implementation MainScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.font = [UIFont cg_fontWithSize:30.0f];
    joinButton.titleLabel.font =[UIFont cg_fontWithSize:30.0f];
    hostButton.titleLabel.font =[UIFont cg_fontWithSize:30.0f];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)hostButtonAction:(id)sender {
    HostViewController *hostViewController = [[HostViewController alloc] initWithNibName:@"HostViewController" bundle:nil];
    hostViewController.delegateHostVC = self;
    [self presentViewController:hostViewController animated:YES completion:nil];
    
}

- (IBAction)joinButtonAction:(id)sender {
    JoinViewController *joinViewController = [[JoinViewController alloc] initWithNibName:@"JoinViewController" bundle:nil];
    joinViewController.delegateJoinVC = self;
    [self presentViewController:joinViewController animated:YES completion:nil];
}

#pragma -mark Delegates method

-(void)hostViewControllerDidCancel:(HostViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)joinViewControllerDidCancel:(JoinViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
