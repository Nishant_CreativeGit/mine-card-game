#import "HostViewController.h"

@interface HostViewController ()

@end


#define SESSION_ID @"MineGame"

@implementation HostViewController

#pragma -mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_matchMakingServer != nil)
        return [_matchMakingServer connectedClientCount];
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSString *peerID = [_matchMakingServer peerIDForConnectedClientAtIndex:indexPath.row];
    cell.textLabel.text = [_matchMakingServer displayNameForPeerID:peerID];
    
    return cell;
}

- (IBAction)closeButtonAction:(id)sender {
    [self.delegateHostVC hostViewControllerDidCancel:self];
}

-(void)matchmakingServer:(MatchServers *)server clientDidConnect:(NSString *)peerID
{
    [self.hostTableView reloadData];
}

-(void)matchmakingServer:(MatchServers *)server clientDidDisconnect:(NSString *)peerID
{
        [self.hostTableView reloadData];
}

#pragma -mark Life Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_matchMakingServer == nil)
    {
        _matchMakingServer = [[MatchServers alloc] init];
        _matchMakingServer.delegateMatchMaking = self;
        _matchMakingServer.maxClients = 3;
        [_matchMakingServer startAcceptingConnectionsForSessionID:SESSION_ID];
        
        self.nameTextField.placeholder = _matchMakingServer.session.displayName;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
