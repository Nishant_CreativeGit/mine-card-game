#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
@class MatchServers;

@protocol MatchmakingServerDelegate <NSObject>

- (void)matchmakingServer:(MatchServers *)server clientDidConnect:(NSString *)peerID;
- (void)matchmakingServer:(MatchServers *)server clientDidDisconnect:(NSString *)peerID;

@end



@interface MatchServers : NSObject <GKSessionDelegate>
{
    
}



#pragma -mark Propertiese

@property (nonatomic, assign) int maxClients;
@property (nonatomic, strong, readonly) NSArray *connectedClients;
@property (nonatomic, strong, readonly) GKSession *session;

#pragma -mark Signature Methods
- (void)startAcceptingConnectionsForSessionID:(NSString *)sessionID;
- (NSUInteger)connectedClientCount;
- (NSString *)peerIDForConnectedClientAtIndex:(NSUInteger)index;
- (NSString *)displayNameForPeerID:(NSString *)peerID;

#pragma -mark Delegate Protocols
@property (nonatomic, weak) id <MatchmakingServerDelegate> delegateMatchMaking;

@end
