#import "UIFont+gameFileAdditions.h"

@implementation UIFont (gameFileAdditions)

+ (id) cg_fontWithSize : (CGFloat)size
{
    return [UIFont fontWithName:@"Action Man" size:size];
}

@end
