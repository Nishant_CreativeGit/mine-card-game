#import "JoinViewController.h"

@interface JoinViewController ()

@end

#define SESSION_ID @"MineGame"
@implementation JoinViewController


-(void)matchmakingClient:(MatchClient *)client serverBecameAvailable:(NSString *)peerID
{
    [self.joinTableView reloadData];
}

-(void)matchmakingClient:(MatchClient *)client serverBecameUnavailable:(NSString *)peerID
{
    [self.joinTableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_matchMakingClient != nil)
        return [_matchMakingClient availableServerCount];
    else
        return 0;
}

#pragma -mark UItableView Delegate Method

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSString *peerID = [_matchMakingClient peerIDForAvailableServerAtIndex:indexPath.row];
    cell.textLabel.text = [_matchMakingClient displayNameForPeerID:peerID];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_matchMakingClient != nil) {
        NSString *peerId = [_matchMakingClient peerIDForAvailableServerAtIndex:indexPath.row];
        [_matchMakingClient connectToServerWithPeerID:peerId];
    }
}

- (IBAction)closeButtonAction:(id)sender {
    [self.delegateJoinVC joinViewControllerDidCancel:self];
}

#pragma -mark Life Cycle Method
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_matchMakingClient == nil)
    {
        _matchMakingClient = [[MatchClient alloc] init];
        _matchMakingClient.delegateMatchMaking = self;
        [_matchMakingClient startSearchingForServersWithSessionID:SESSION_ID];
        
        self.nameTextField.placeholder = _matchMakingClient.session.displayName;
        [self.joinTableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end