//
//  MainScreenViewController.h
//  MineCardGame
//
//  Created by cgsadmin on 3/23/16.
//  Copyright © 2016 cgsadmin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HostViewController.h"
#import "JoinViewController.h"

@interface MainScreenViewController : UIViewController<hostViewControllerDelegate,joinViewControllerDelegate>
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *hostButton;
    IBOutlet UIButton *joinButton;
}
- (IBAction)hostButtonAction:(id)sender;
- (IBAction)joinButtonAction:(id)sender;


@end
