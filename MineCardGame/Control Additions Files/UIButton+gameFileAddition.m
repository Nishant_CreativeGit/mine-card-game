#import "UIButton+gameFileAddition.h"
#import "UIFont+gameFileAdditions.h"

@implementation UIButton (gameFileAddition)

-(void)cg_applyButtonStyle
{
    self.titleLabel.font = [UIFont cg_fontWithSize:30.0f];
}

@end
