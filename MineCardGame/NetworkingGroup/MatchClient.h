#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
@class MatchClient;


#pragma -mark Protocol Method

@protocol MatchmakingClientDelegate <NSObject>

- (void)matchmakingClient:(MatchClient *)client serverBecameAvailable:(NSString *)peerID;
- (void)matchmakingClient:(MatchClient *)client serverBecameUnavailable:(NSString *)peerID;

@end

@interface MatchClient : NSObject <GKSessionDelegate>
{
    NSMutableArray *_availableServers;
}

#pragma -mark Propertiese Method
@property (nonatomic, strong, readonly) NSArray *availableServers;
@property (nonatomic, strong, readonly) GKSession *session;

#pragma -mark Method Signatures
- (NSUInteger)availableServerCount;
- (NSString *)peerIDForAvailableServerAtIndex:(NSUInteger)index;
- (NSString *)displayNameForPeerID:(NSString *)peerID;

-(void)connectToServerWithPeerID :(NSString *)peerId;

#pragma -mark Delegate Propertiese and Methods

@property (nonatomic, weak) id <MatchmakingClientDelegate> delegateMatchMaking;
- (void)startSearchingForServersWithSessionID:(NSString *)sessionID;

@end
